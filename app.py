import boto3
import os

def upload_handler():
    s3_client = boto3.client("s3")

    filepath = '/tmp/RandomData.dat'
    f = open(filepath,"wb")
    f.seek((1024 * 1024 * 1024 * 1024) - 1)
    f.write(b"\0")
    f.close()
    os.stat(filepath).st_size
    response = s3_client.upload_file(filepath, "public-junk-129908582495", "my_file_remote_1TB_fargate")
    return response

if __name__ == "__main__":
    upload_handler()